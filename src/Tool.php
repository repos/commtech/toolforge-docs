<?php

namespace App;

class Tool {

	/** @var int */
	private $id;

	/** @var string */
	private $name;

	/** @var string */
	private $title;

	/** @var string */
	private $description;

	/** @var string */
	private $repository;

	/**
	 * @param int $id
	 * @param string $name
	 * @param string $title
	 * @param string $description
	 * @param string $repository
	 */
	public function __construct( int $id, string $name, string $title, string $description, string $repository ) {
		$this->id = $id;
		$this->name = $name;
		$this->title = $title;
		$this->description = $description;
		$this->repository = $repository;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $repository
	 */
	public function setRepository( string $repository ): void {
		$this->repository = $repository;
	}

	/**
	 * @return string
	 */
	public function getRepository(): string {
		return $this->repository;
	}

	/**
	 * @return string
	 */
	public function getGitlabProjectPath(): ?string {
		if ( !$this->repository ) {
			return null;
		}
		preg_match( '|https://gitlab.wikimedia.org/([^\.]*)|', $this->getRepository(), $matches );
		return $matches[1] ?? null;
	}
}
