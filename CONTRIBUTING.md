Contributing to Tools' Documentation Viewer
===========================================

ToolDocs uses the Symfony framework, Symfony web server, and Bootstrap 5.
Visit the [workboard](https://phabricator.wikimedia.org/tag/tool-docs/) to report a bug, propose a feature, or ask questions.

## First-time setup

Before you start, make sure you have PHP, Composer, Symfony, and MariaDB installed.

1. Install dependencies: `composer install`
2. Start the database: `/usr/local/bin/mysqld_safe --skip-grant-tables`
3. Copy the environment config: `cp .env .env.local`
   * Edit `DATABASE_URL` with your database credentials.
   * Create a new [GitLab access token](https://gitlab.wikimedia.org/-/profile/personal_access_tokens)
     and add it as `GITLAB_ACCESS_TOKEN`.
4. Create the database: `./bin/console d:d:c`
5. Run migrations, and create the schema: `./bin/console d:m:m`

## Local development

### Populating the docs
```bash
# Fetch tools from Toolhub
./bin/console app:download
# Clone repositories, and import markdown files
./bin/console app:import
```

### Running the local server

Set `APP_ENV=dev` in .env.local to update the local server automatically.

```bash
# Start a local web server in the background
symfony server:start -d 
```

### Clearing the cache
Clear the cache to update the local server after making changing to php files or .env.

```bash
./bin/console cache:clear
```

### Troubleshooting

For errors about resolving packages, run:

```bash
composer install
```

## CLI Usage

### app:download

Retrieve tool information from Toolhub.

    app:download


### app:import

Clone repositories and import documentation.

    app:import [-t|--tool TOOL]

* `--tool` `-t` — Import only this tool.
  This option can be provided multiple times.
