module.exports = {
    "extends": "wikimedia/client-es6",
    env: {
        browser: true,
        node: true,
        es6: true
    },
};
