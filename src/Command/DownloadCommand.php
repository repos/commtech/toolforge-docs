<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DownloadCommand extends Command {

	/** @var string */
	protected static $defaultName = 'app:download';

	/** @var string */
	protected static $defaultDescription = 'Retrieve tool information from Toolhub.';

	/** @var HttpClientInterface */
	private $client;

	/** @var Connection */
	private $connection;

	/**
	 * @param HttpClientInterface $client
	 * @param Connection $connection
	 */
	public function __construct( HttpClientInterface $client, Connection $connection ) {
		parent::__construct();
		$this->client = $client;
		$this->connection = $connection;
	}

	protected function configure(): void {
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$insertToolStmt = $this->connection->prepare(
			'INSERT INTO tools
			SET name=:name, title=:title, description=:description
			ON DUPLICATE KEY UPDATE name=:name, title=:title, description=:description'
		);
		$insertRepositoryStmt = $this->connection->prepare(
			'INSERT INTO repositories
			SET tool_id=:tool_id, url=:url
			ON DUPLICATE KEY UPDATE url=:url'
		);
		$selectToolStmt = $this->connection->prepare(
			'SELECT id FROM tools WHERE name = :name LIMIT 1'
		);
		$insertExternalDocs = $this->connection->prepare(
			'INSERT INTO external_docs SET tool_id=:tool_id, url=:url, lang=:lang
			ON DUPLICATE KEY UPDATE url=:url'
		);
		$pageNum = 1;
		do {
			$toolhubUrl = 'https://toolhub.wikimedia.org/api/tools/?page_size=100&page=' . $pageNum;
			$data = $this->client->request( 'GET', $toolhubUrl )->toArray();
			foreach ( $data['results'] as $tool ) {
				// Only include tools with documentation links or code repositories.
				if ( !isset( $tool['user_docs_url'][0] ) && !isset( $tool['repository'] ) ) {
					continue;
				}

				$output->write( "Saving " . $tool['name'] );
				$insertToolStmt->bindValue( 'name', $tool['name'] );
				$insertToolStmt->bindValue( 'title', $tool['title'] );
				$insertToolStmt->bindValue( 'description', $tool['description'] );
				$insertToolStmt->executeStatement();
				$toolId = $selectToolStmt->executeQuery( [ 'name' => $tool['name'] ] )->fetchOne();

				// Save repository details.
				if ( $tool['repository'] ) {
					$output->write( ' -- repository: ' . $tool['repository'] );
					$insertRepositoryStmt->executeStatement( [ 'tool_id' => $toolId, 'url' => $tool['repository'] ] );
				} else {
					$output->write( ' -- no repository' );
				}
				$output->writeln( '' );

				// Save all external doc URLs.
				foreach ( $tool['user_docs_url'] as $docUrl ) {
					// Each doc URL can be either a string, or a url/lang array.
					$insertExternalDocs->executeStatement( [
						'tool_id' => $toolId,
						'url' => $docUrl['url'] ?? $docUrl,
						'lang' => $docUrl['language'] ?? 'en',
					] );
				}

			}
			$pageNum++;
		} while ( isset( $data['next'] ) );

		return Command::SUCCESS;
	}
}
