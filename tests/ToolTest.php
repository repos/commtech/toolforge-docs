<?php

namespace App\Test;

use App\Tool;
use PHPUnit\Framework\TestCase;

class ToolTest extends TestCase {

	/**
	 * @dataProvider provideRepo
	 * @covers \App\Page::buildToc()
	 */
	public function testRepo( $repo, $project ) {
		$tool = new Tool( 0, '', '', '', $repo );
		$this->assertSame( $project, $tool->getGitlabProjectPath() );
	}

	public function provideRepo() {
		return [
			[ 'https://gitlab.wikimedia.org/foobar', 'foobar' ],
			[ 'https://gitlab.wikimedia.org/foobar.git', 'foobar' ],
			[ 'https://gitlab.wikimedia.org/foo/bar', 'foo/bar' ],
			[ 'https://gitlab.wikimedia.org/foo/bar.git', 'foo/bar' ],
			[ 'https://github.com/foo/bar.git', null ],
		];
	}
}
