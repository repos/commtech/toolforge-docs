<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220510014400 extends AbstractMigration {

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Add repositories table';
	}

	/**
	 * @param Schema $schema
	 */
	public function up( Schema $schema ): void {
		$this->addSql( '
			CREATE TABLE IF NOT EXISTS `repositories` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`tool_id` INT(10) UNSIGNED NOT NULL,
				`url` VARCHAR(500) NOT NULL,
				CONSTRAINT FK_REPOSITORIES_TOOL FOREIGN KEY ( `tool_id` ) REFERENCES `tools` ( `id` ) ON DELETE CASCADE,
				UNIQUE INDEX UNIQ_REPOSITORIES_TOOL (`tool_id`)
			) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB
		;' );
		$this->addSql( 'INSERT INTO repositories ( id, tool_id, url )
			SELECT t.id, t.id, "" FROM tools t JOIN docs d ON ( t.id = d.tool_id )
		;' );
		$this->addSql( 'ALTER TABLE `docs`
			DROP FOREIGN KEY `docs_ibfk_1`,
			DROP INDEX `UNIQ_DOCS_TOOL_LANG_VERSION`
		;' );
		$this->addSql( 'ALTER TABLE `docs`
			CHANGE `tool_id` `repository_id` INT(10) UNSIGNED NOT NULL,
			ADD UNIQUE `UNIQ_DOCS_REPOSITORY_LANG_VERSION` (`repository_id`, `lang`, `version`)
		;' );
		$this->addSql( 'ALTER TABLE `docs`
			ADD CONSTRAINT `FK_DOCS_REPOSITORY` FOREIGN KEY ( `repository_id` )
				REFERENCES `repositories` ( `id` ) ON DELETE CASCADE ON UPDATE RESTRICT
		;' );
	}

	/**
	 * @param Schema $schema
	 */
	public function down( Schema $schema ): void {
		$this->addSql( 'ALTER TABLE `docs`
			DROP FOREIGN KEY `FK_DOCS_REPOSITORY`,
			DROP INDEX `UNIQ_DOCS_REPOSITORY_LANG_VERSION`
		;' );
		$this->addSql( 'ALTER TABLE `docs`
			CHANGE `repository_id` `tool_id` INT(10) UNSIGNED NOT NULL,
			ADD UNIQUE `UNIQ_DOCS_TOOL_LANG_VERSION` (`tool_id`, `lang`, `version`),
			ADD CONSTRAINT `docs_ibfk_1` FOREIGN KEY ( `tool_id` )
				REFERENCES `tools` ( `id` ) ON DELETE CASCADE ON UPDATE RESTRICT
		;' );
		$this->addSql( 'DROP TABLE IF EXISTS `repositories`' );
	}
}
