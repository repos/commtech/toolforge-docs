---
title: Adding a project
---
As no documentation is actually stored within ToolDocs,
it is necessary to first create a Git repository in which to store the documentation source.
This is usually the same repository as a tool's software source code,
and we recommend using [Wikimedia GitLab](https://gitlab.wikimedia.org).

ToolDocs will attempt to autodiscover your repository and documentation via Toolhub,
so if your tool is registered there with the repository URL, there should be no further action required.

After the project has been added,
ToolDocs will clone your repository and build the documentation,
making it available at `docs.toolforge.org/docs/tool-name`.
This process runs every three hours, and will also check for correct structure of the files (see below).

## Documentation structure

Documentation files should be in a `docs/` directory in your project repository,
and within that grouped by language code.

The structure within each language directory is completely up to you.
The only requirement is that there is an `index.md` file in each directory.
The file names and directory structure will be used in the URLs for the published documentation pages.

For general information about writing documentation,
the [Write the Docs](https://www.writethedocs.org/) website is a useful resource.

## Frontmatter

If you add YAML frontmatter to a documentation Markdown file,
it will be parsed and some parts used for improving the published docs.

For example, if the following frontmatter is in a file called `docs/en/intro.md`:

```
---
title: Introduction
---
This is the introductory documentation.
```

Then the title of the page will be 'Introduction'
(in the window titlebar, at the top of the page, and in the table of contents).

## Linking between pages

ToolDocs does no special handling of links in Markdown,
so it's important to keep in mind the URL context of the published docs.
This basically comes down to two rules:

1. Always use relative URLs, e.g. `[Lorem ipsum](../lorem/ipsum)`.
2. Do not include `.html` or any other file extension.

## [Under construction] Adding a project manually

If you want to add a project manually, first log in via the main menu
(you will be redirected to Meta-Wiki to authorize ToolDocs, and then back again),
then enter your project's Git URL in the "New project:" form field on the homepage.
The HTTPS URL should be used (rather than an SSH one), for example: `https://gitlab.wikimedia.org/exampleuser/example-project.git`.

This will add your project to the system, and give you access to an admin area in which you can change your project's settings.
If your tool has already been added automatically from Toolhub, you will have access to this admin area
if your username on ToolDocs matches one listed as an author of the tool on Toolhub.
