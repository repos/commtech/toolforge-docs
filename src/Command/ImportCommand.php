<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Krinkle\Intuition\Intuition;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ImportCommand extends Command {

	/** @var string */
	protected static $defaultName = 'app:import';

	/** @var string */
	protected static $defaultDescription = 'Clone repositories and import documentation.';

	/** @var Connection */
	private $connection;

	/** @var string */
	private $projectDir;

	/** @var Intuition */
	private $intuition;

	/** @var SymfonyStyle */
	private $io;

	/** @var SymfonyStyle */
	private $err;

	/**
	 * @param Connection $connection
	 * @param string $projectDir
	 * @param Intuition $intuition
	 */
	public function __construct( Connection $connection, string $projectDir, Intuition $intuition ) {
		parent::__construct();
		$this->connection = $connection;
		$this->projectDir = $projectDir;
		$this->intuition = $intuition;
	}

	protected function configure(): void {
		$this->addOption(
			'tool', 't', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'Import only this tool.', []
		);
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$this->io = new SymfonyStyle( $input, $output );
		$this->err = new SymfonyStyle( $input, $output->getErrorOutput() );

		$qb = $this->connection->createQueryBuilder();
		$qb->select( 'r.url', 't.name', 'r.id AS repository_id' )
			->from( 'repositories', 'r' )
			->join( 'r', 'tools', 't', 'r.tool_id = t.id' );
		$params = [];
		if ( $input->getOption( 'tool' ) ) {
			$qb->where( $qb->expr()->in( 't.name', ':tools' ) );
			$qb->setParameter( 'tools', $input->getOption( 'tool' ), Connection::PARAM_STR_ARRAY );
		}
		$repositories = $qb->executeQuery( $params )->fetchAllAssociative();
		$deleteDocs = $this->connection->prepare( 'DELETE FROM docs WHERE repository_id=:repository_id' );
		foreach ( $repositories as $data ) {
			$this->io->title( 'Tool: ' . $data['name'] );
			$repoDir = $this->cloneRepo( $data );
			if ( $repoDir === '' ) {
				continue;
			}
			$this->io->writeln( 'Repo: ' . basename( $repoDir ) );

			// Clean old docs.
			$repositoryId = (int)$data['repository_id'];
			$deleteDocs->executeStatement( [ 'repository_id' => $repositoryId ] );

			// Import default branch.
			$gitBranchMain = [ 'git', 'symbolic-ref', '--short', '-q', 'refs/remotes/origin/HEAD' ];
			$errMsgSuffix = 'default branch of ' . $data['name'] . ': ' . $data['url'];
			try {
				$defaultBranch = trim( ( new Process( $gitBranchMain, $repoDir ) )->mustRun()->getOutput() );
			} catch ( ProcessFailedException $exception ) {
				$this->err->error( "ERROR: Unable to run command to determine $errMsgSuffix" );
				$this->err->block( $exception->getMessage() );
				continue;
			}
			if ( !$defaultBranch ) {
				$this->err->error( "ERROR: Unable to determine $errMsgSuffix" );
				continue;
			}
			$defaultBranchName = substr( $defaultBranch, strpos( $defaultBranch, '/' ) + 1 );
			( new Process( [ 'git', 'checkout', '-f', $defaultBranchName ], $repoDir ) )->mustRun();
			( new Process( [ 'git', 'reset', '--hard', $defaultBranch ], $repoDir ) )->mustRun();
			$this->io->writeln( 'Importing latest: ' . $defaultBranch );
			$this->importDocs( $repositoryId, $repoDir, 'latest' );

			// Import tags.
			$this->importTags( $repositoryId, $repoDir );
		}
		return Command::SUCCESS;
	}

	/**
	 * @param string[] $repo
	 * @return string|null The full filesystem path to the cloned repository directory.
	 */
	protected function cloneRepo( array $repo ): ?string {
		$url = $repo['url'];
		// Multiple tools can have the same repo, but we don't want to duplicate it locally.
		$repoDir = $this->projectDir . '/var/repos/' . md5( $url );
		if ( !is_dir( $repoDir ) ) {
			$this->io->writeln( 'Cloning ' . $repo['name'] . "\n    from $url\n    to $repoDir" );
			try {
				$command = [ 'git', 'clone', $url, $repoDir ];
				( new Process( $command, null, [ 'GIT_TERMINAL_PROMPT' => 0 ] ) )->mustRun();
			} catch ( ProcessFailedException $exception ) {
				$this->err->error( 'ERROR: Unable to clone from ' . $url );
				$this->err->block( $exception->getMessage() );
				return null;
			}
		} else {
			$this->io->writeln( 'Updating from ' . $url );
			try {
				( new Process( [ 'git', 'fetch', '--tags' ], $repoDir ) )->setTimeout( 60 * 10 )->mustRun();
			} catch ( ProcessFailedException $exception ) {
				$this->err->error( 'ERROR: Unable to update from ' . $url );
				$this->err->block( $exception->getMessage() );
				return null;
			}
		}
		return $repoDir;
	}

	/**
	 * @param int $repositoryId
	 * @param string $repoDir
	 */
	private function importTags( int $repositoryId, string $repoDir ): void {
		$tagList = ( new Process( [ 'git', 'tag' ], $repoDir ) )->mustRun()->getOutput();
		$tags = array_filter( explode( "\n", $tagList ) );
		foreach ( $tags as $tag ) {
			$this->io->writeln( 'Importing tag: ' . $tag );
			( new Process( [ 'git', 'checkout', '-f', $tag ], $repoDir ) )->mustRun();
			$this->importDocs( $repositoryId, $repoDir, $tag );
		}
	}

	/**
	 * @param int $repositoryId
	 * @param string $repoDir
	 * @param string $version
	 */
	private function importDocs( int $repositoryId, string $repoDir, string $version ): void {
		$docsDir = $repoDir . '/docs';
		if ( !file_exists( $docsDir ) ) {
			$this->io->writeln( '  <error>No docs/ directory found</error>' );
			return;
		}
		$finder = new Finder();
		$markdownFiles = $finder
			->files()
			->in( $repoDir . '/docs' )
			->name( '*.md' );
		foreach ( $markdownFiles as $markdownFile ) {
			if ( $this->saveFile( $repositoryId, $version, $markdownFile ) ) {
				$msg = '  File imported: <info>' . $markdownFile->getRelativePathname() . '</info>';
				$this->io->writeln( $msg );
			}
		}
	}

	/**
	 * Save a single markdown file's info and contents to the database.
	 * @param string $repositoryId
	 * @param string $version
	 * @param SplFileInfo $file
	 * @return bool Whether the file was imported.
	 */
	private function saveFile( string $repositoryId, string $version, SplFileInfo $file ): bool {
		$pathParts = array_filter( explode( DIRECTORY_SEPARATOR, $file->getRelativePath() ) );
		$pathParts[] = $file->getFilenameWithoutExtension();
		if ( count( $pathParts ) < 2 ) {
			// Needs at least lang and filename.
			return false;
		}
		// Make sure the lang code is valid (according to Intuition's list).
		$lang = substr( $pathParts[0], 0, 10 );
		$langName = $this->intuition->getLangName( $lang );
		if ( !$langName ) {
			$msg = "  <error>Invalid language code '$lang' in " . $file->getRelativePathname() . "</error>";
			$this->io->writeln( $msg );
			return false;
		}
		unset( $pathParts[0] );
		$path = '/' . implode( '/', $pathParts );

		// Save tool lang and version.
		$docIdKey = $repositoryId . $lang . $version;
		if ( !isset( $this->docIds[$docIdKey] ) ) {
			$params = [ 'repository_id' => $repositoryId, 'lang' => $lang, 'version' => $version ];
			$insertDocs = 'INSERT IGNORE INTO `docs`
				SET `repository_id`=:repository_id, `lang`=:lang, `version`=:version';
			$this->connection->executeStatement( $insertDocs, $params );
			$docIdSelect = 'SELECT id FROM `docs`
				WHERE `repository_id`=:repository_id AND `lang`=:lang AND `version`=:version';
			$docId = $this->connection->executeQuery( $docIdSelect, $params )->fetchOne();
			$this->docIds[$docIdKey] = $docId;
		}
		$docId = $this->docIds[$docIdKey];

		// Save page.
		$pagesSql = 'INSERT INTO pages SET doc_id=:doc_id, path=:path, contents=:contents'
			. ' ON DUPLICATE KEY UPDATE contents=:contents';
		$this->connection->executeStatement(
			$pagesSql,
			[ 'doc_id' => $docId, 'path' => $path, 'contents' => $file->getContents() ]
		);
		return true;
	}
}
