<?php

namespace App\Controller;

use App\Repository;
use Gitlab\Client;
use Gitlab\Exception\RuntimeException as GitlabRuntimeException;
use Krinkle\Intuition\Intuition;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
// phpcs:ignore MediaWiki.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController {

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/edit/{toolName}/{lang}/{path}",
	 *        name="edit",
	 *        requirements={"path"=".*"}
	 * )
	 * @param Repository $repository
	 * @param string $toolName
	 * @param string $lang
	 * @param string $path
	 * @return Response
	 */
	public function edit( Repository $repository, string $toolName, string $lang, string $path ): Response {
		$tool = $repository->getTool( $toolName );
		$page = $repository->getPage( $tool, $lang, $path );
		return $this->render( 'edit.html.twig', [
			'tool' => $tool,
			'page' => $page,
			'file_path' => 'docs/' . $lang . '/' . $page->getPath() . '.md',
		] );
	}

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/translations/{toolName}", name="translations" )
	 * @param Repository $repository
	 * @param Intuition $intuition
	 * @param string $toolName
	 * @return Response
	 */
	public function translations( Repository $repository, Intuition $intuition, string $toolName ): Response {
		$tool = $repository->getTool( $toolName );
		$langs = array_unique( array_merge( [ $intuition->getLang() ], $repository->getToolsLangs( $tool ) ) );
		sort( $langs );
		return $this->render( 'translations.html.twig', [
			'full_width' => true,
			'tool' => $tool,
			'langs' => $langs,
			'pages' => $repository->getPagePaths( $tool ),
		] );
	}

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route( "/translate/{toolName}/{path}", name="translate", requirements={"path"=".*"} )
	 * @param Session $session
	 * @param Repository $repository
	 * @param Intuition $intuition
	 * @param Client $gitlab
	 * @param string $toolName
	 * @param string $path
	 * @return Response
	 */
	public function translate(
		Session $session, Repository $repository, Intuition $intuition, Client $gitlab, string $toolName, string $path
	): Response {
		$tool = $repository->getTool( $toolName );
		$sourcePage = $repository->getPage( $tool, 'en', $path );
		$lang = $intuition->getLang();
		$targetPage = $repository->getPage( $tool, $lang, $path );

		// See if the user has an edit in progress for this file, and if so use the contents from that.
		$user = $session->get( 'logged_in_user' );
		if ( $user ) {
			$username = str_replace( ' ', '_', $session->get( 'logged_in_user' )->username );
			try {
				$existingFile = $gitlab
					->repositoryFiles()
					->getFile( $tool->getGitlabProjectPath(), "docs/$lang/$path.md", "tooldocs-$username" );
				$targetPage->setContents( base64_decode( $existingFile['content'] ) );
			} catch ( GitlabRuntimeException $ex ) {
				// File not found, do nothing.
			}
		}

		return $this->render( 'translate.html.twig', [
			'full_width' => true,
			'tool' => $tool,
			'source_page' => $sourcePage,
			'target_page' => $targetPage,
		] );
	}

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/save", name="save")
	 * @param Repository $repository
	 * @param Session $session
	 * @param Request $request
	 * @param Client $gitlab
	 * @return Response
	 */
	public function save( Repository $repository, Session $session, Request $request, Client $gitlab ): Response {
		$submittedToken = $request->request->get( 'token' );
		if ( !$session->get( 'logged_in_user' ) || !$this->isCsrfTokenValid( 'edit-page', $submittedToken ) ) {
			throw $this->createAccessDeniedException();
		}
		$username = str_replace( ' ', '_', $session->get( 'logged_in_user' )->username );

		$tool = $repository->getTool( $request->request->get( 'tool' ) );
		$lang = $request->request->get( 'lang' );

		// File and branches.
		$projectInfo = $gitlab->projects()->show( $tool->getGitlabProjectPath() );
		$projectId = $projectInfo['id'];
		$defaultBranch = $projectInfo['default_branch'];
		$page = $repository->getPage( $tool, $lang, $request->request->get( 'path' ) );
		$filePath = 'docs/' . $lang . '/' . $page->getPath() . '.md';
		$newBranch = "tooldocs-$username";

		// Set up the edit.
		$save = [
			'branch' => $newBranch,
			'author_name' => "User:$username",
			'file_path' => $filePath,
			'content' => $request->request->get( 'contents' ),
			'commit_message' => "Updates to " . $page->getPath() . " ($lang) via ToolDocs, by [[User:$username]]",
		];

		// See if the user branch exists, and if it doesn't set the 'start_branch'. This creates a new branch.
		try {
			$gitlab->repositories()->branch( $projectId, $newBranch );
		} catch ( GitlabRuntimeException $ex ) {
			// 404 Branch Not Found
			$save['start_branch'] = $defaultBranch;
		}

		// Save the new file contents.
		try {
			$gitlab->repositoryFiles()->getFile( $projectId, $filePath, $save['start_branch'] ?? $save['branch'] );
			$gitlab->repositoryFiles()->updateFile( $projectId, $save );
		} catch ( GitlabRuntimeException $ex ) {
			$gitlab->repositoryFiles()->createFile( $projectId, $save );
		}

		// Find any open MR with this branch.
		$existingMergeRequests = $gitlab->mergeRequests()->all( $projectId, [
			'state' => 'opened',
			'source_branch' => $newBranch,
		] );
		if ( count( $existingMergeRequests ) === 0 ) {
			$gitlab->mergeRequests()->create( $projectId, $newBranch, $defaultBranch, 'Updates from ToolDocs', [] );
		}

		return $this->redirectToRoute( 'page', [
			'tool' => $tool->getName(),
			'lang' => $lang,
			'path' => $page->getPath(),
			'version' => 'latest',
		] );
	}
}
