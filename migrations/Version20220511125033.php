<?php

declare ( strict_types = 1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220511125033 extends AbstractMigration {

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Add FullText Index to Pages Content';
	}

	/**
	 * @param Schema $schema
	 */
	public function up( Schema $schema ): void {
		$this->addSql( 'CREATE FULLTEXT INDEX IDX_PAGES_CONTENT
                ON `pages`( contents )
		;' );
		$this->addSql( 'CREATE FULLTEXT INDEX IDX_TOOLS_TITLE
                ON `tools`( title )
        ;' );
	}

	/**
	 * @param Schema $schema
	 */
	public function down( Schema $schema ): void {
		$this->addSql( 'ALTER TABLE `pages`
                DROP INDEX IDX_PAGES_CONTENT
          ;' );

		$this->addSql( 'ALTER TABLE `tools`
            	DROP INDEX IDX_TOOLS_TITLE
          ;' );
	}
}
