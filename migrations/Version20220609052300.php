<?php

declare ( strict_types = 1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220609052300 extends AbstractMigration {

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Add fulltext index to tools.description';
	}

	/**
	 * @param Schema $schema
	 */
	public function up( Schema $schema ): void {
		$this->addSql( 'CREATE FULLTEXT INDEX IDX_TOOLS_DESCRIPTION ON `tools`( `description` );' );
	}

	/**
	 * @param Schema $schema
	 */
	public function down( Schema $schema ): void {
		$this->addSql( 'ALTER TABLE `tools` DROP INDEX IDX_TOOLS_DESCRIPTION;' );
	}
}
