<?php

namespace App\Controller;

use App\Page;
use App\Repository;
use Doctrine\DBAL\Connection;
use Gitlab\Client;
use Krinkle\Intuition\Intuition;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
// phpcs:ignore MediaWiki.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\Routing\Annotation\Route;

class DocsController extends AbstractController {

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/", name="home")
	 * @param Connection $connection
	 * @param Intution $intuition
	 * @return Response
	 */
	public function home( Connection $connection, Intuition $intuition ): Response {
		$sql = 'SELECT t.name, t.title, t.description, d.lang, d.version
			FROM tools t
				LEFT JOIN repositories r ON ( t.id = r.tool_id )
				LEFT JOIN docs d ON ( r.id = d.repository_id )
			WHERE d.lang = :lang
			GROUP BY t.id
			ORDER BY t.title ASC';
		$tools = $connection
			->executeQuery( $sql, [ 'lang' => $intuition->getLang() ] )
			->fetchAllAssociative();
		$langsSql = 'SELECT lang FROM docs GROUP BY lang';
		$langs = $connection->executeQuery( $langsSql )->fetchFirstColumn();
		return $this->render( 'home.html.twig', [
			'langs' => $langs,
			'tools' => $tools,
		] );
	}

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/docs/{tool}/{lang}/{version}/{path}",
	 *        name="page",
	 *        requirements={"tool"="^(?!_)[^/]+", "page"="[^/]*", "version"="[^/]*", "path"=".*"}
	 * )
	 * @param Request $request
	 * @param Intuition $intuition
	 * @param Connection $connection
	 * @param Repository $repository
	 * @param Client $gitlab
	 * @param Session $session
	 * @param string $tool
	 * @param string $lang
	 * @param string $version
	 * @param string $path
	 * @return Response
	 */
	public function page(
		Request $request, Intuition $intuition, Connection $connection, Repository $repository, Client $gitlab,
		Session $session, string $tool, string $lang = '', string $version = '', string $path = ''
	): Response {
		if ( !$lang || !$intuition->getLangName( $lang ) ) {
			$lang = 'en';
		}
		$intuition->setLang( $lang );
		if ( !$version ) {
			$version = 'latest';
		}
		if ( !$path ) {
			$path = 'index';
		}
		$routeParams = [
			'tool' => $tool,
			'path' => $path,
			'version' => $version,
			'lang' => $lang,
		];
		if ( $request->getRequestUri() !== $this->generateUrl( 'page', $routeParams ) ) {
			return $this->redirectToRoute( 'page', $routeParams );
		}

		$toolData = $repository->getTool( $tool );
		if ( !$toolData->getId() ) {
			// @TODO Translate error message.
			throw $this->createNotFoundException( 'Tool not found: ' . $tool );
		}

		$response = null;
		$page = $connection->executeQuery( '
			SELECT d.lang, d.version, p.path, p.contents FROM pages p
				JOIN docs d ON ( p.doc_id = d.id )
				JOIN repositories r ON ( d.repository_id = r.id )
			WHERE path=:path AND version=:version AND lang=:lang AND r.tool_id=:tool_id
			LIMIT 1
			', [
				'path' => "/$path",
				'version' => $version,
				'lang' => $lang,
				'tool_id' => $toolData->getId(),
			] )->fetchAssociative();
		if ( !$page ) {
			// @TODO Improve the not-found handling.
			$p = new Page( $version, $lang, $path, '' );
			$body = '<p>Page not found.</p>';
			$frontmatter = [
				'title' => 'Page not found.',
			];
			$response = new Response( 'Not found', Response::HTTP_NOT_FOUND );
		} else {
			$p = new Page( $version, $lang, $path, $page['contents'] );
			$frontmatter = $p->getMetadata();
			$body = $p->getHtml();
		}

		$qb = $connection->createQueryBuilder();
		$qb->from( 'docs', 'd' )
			->join( 'd', 'repositories', 'r', 'd.repository_id = r.id' )
			->where( 'r.tool_id = :tool_id' )
			->setParameter( 'tool_id', $toolData->getId() );
		$versions = $qb->select( 'd.version' )
			->groupBy( 'd.version' )
			->orderBy( 'd.version', 'DESC' )
			->executeQuery()
			->fetchFirstColumn();
		$langs = $qb->select( 'd.lang' )
			->groupBy( 'd.lang' )
			->orderBy( 'd.lang', 'DESC' )
			->executeQuery()
			->fetchFirstColumn();

		// Get pages.
		// @TODO Replace with a proper ToC system.
		$pages = $connection->executeQuery( '
			SELECT DISTINCT SUBSTR(p.path, 2)
				FROM pages p
				JOIN docs d ON ( p.doc_id = d.id )
				JOIN repositories r ON ( d.repository_id = r.id )
			WHERE r.tool_id=:tool_id AND d.lang=:lang AND d.version=:version
			ORDER BY p.path DESC
			;', [
				'tool_id' => $toolData->getId(),
				'lang' => $lang,
				'version' => $version,
			] )->fetchFirstColumn();

		$mergeRequest = false;
		if ( $session->get( 'logged_in_user' ) ) {
			// @TODO Move this to a GitLab repository class, and cache it for a few minutes.
			$branchName = 'tooldocs-' . str_replace( ' ', '_', $session->get( 'logged_in_user' )->username );
			$mergeRequest = $gitlab->mergeRequests()->all( $toolData->getGitlabProjectPath(), [
				'state' => 'opened',
				'source_branch' => $branchName,
			] )[0] ?? false;
		}

		return $this->render( 'page.html.twig', [
			'tool' => $toolData,
			'page' => $p,
			'versions' => $versions,
			'langs' => $langs,
			'pages' => $p->buildToc( $pages ),
			'frontmatter' => $frontmatter,
			'html' => $body,
			'merge_request' => $mergeRequest,
		], $response );
	}
}
