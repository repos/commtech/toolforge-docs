---
title: ToolDocs Docs
---
[ToolDocs](https://docs.toolforge.org) is a documentation viewer for Wikimedia tools.

Features:

* Store docs alongside code
* Full-text search across tools
* Versioning
* Support for multiple languages

## How ToolDocs works

1. [Write the docs](./adding-a-project) in your tool's code repository.
2. Register your tool on [Toolhub](https://toolhub.wikimedia.org/).
3. Read the docs at `docs.toolforge.org/docs/your-tool-name`.

## Who ToolDocs is for

| Good for | Not good for |
|----------|--------------|
| ✅ Tools that use a code repository | ❌ Tools that store code on a wiki |
| ✅ Simple, narrative docs | ❌ Generated docs or complex formatting |
|   | ❌ Tools already documented on a wiki |

## Get started

**[Add a project](./adding-a-project)** to ToolDocs.

## Philosophy

ToolDocs is an experiment in managing documentation using the same tools and workflows as the code it documents.
This is known as the docs-as-code approach.
It involves storing documentation as text files (usually [Markdown](https://www.markdownguide.org/getting-started/))
within the same repository as the main software code,
and keeping this documentation up to date as the code changes.
The documentation files are then compiled to HTML and made available on the web.

The primary advantages of this approach are that the documentation only ever needs to address the current state of the code
(i.e. doesn't repeatedly have to call out differences of behaviour between versions),
and continuous-integration processes can be put in place to help ensure that
every code change is either accompanied by a documentation change or a declaration that no such change is required.

Common tools for docs-in-code are [Sphinx](https://www.sphinx-doc.org/) and [MkDocs](https://www.mkdocs.org/).
The former is the basis for the popular [Read the Docs](https://readthedocs.org/) platform,
and the latter is used for the new Wikimedia [Developer Portal](https://www.mediawiki.org/wiki/Developer_Advocacy/Developer_Portal).

## History

ToolDocs was started as part of the Wikimedia Foundation CommTech Hackathon and Wikimedia Hackathon in May 2022.
