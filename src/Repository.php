<?php

namespace App;

use Doctrine\DBAL\Connection;

class Repository {

	/** @var Connection */
	private $db;

	/**
	 * @param Connection $db
	 */
	public function __construct( Connection $db ) {
		$this->db = $db;
	}

	/**
	 * @param string $name
	 * @return Tool
	 */
	public function getTool( string $name ): Tool {
		$toolSql = 'SELECT t.id, t.name, t.title, t.description, r.url AS repository
			FROM tools t JOIN repositories r ON ( t.id = r.tool_id )
			WHERE name=?';
		$toolData = $this->db->executeQuery( $toolSql, [ $name ] )->fetchAssociative();
		return new Tool(
			(int)$toolData['id'],
			$toolData['name'],
			$toolData['title'],
			$toolData['description'],
			$toolData['repository'],
		);
	}

	/**
	 * @param Tool $tool
	 * @param string $lang
	 * @param string $path
	 * @return Page
	 */
	public function getPage( Tool $tool, string $lang, string $path ): Page {
		$pageData = $this->db->executeQuery( '
		SELECT d.lang, d.version, p.path, p.contents FROM pages p
			JOIN docs d ON ( p.doc_id = d.id )
			JOIN repositories r ON ( d.repository_id = r.id )
		WHERE path=:path AND version="latest" AND lang=:lang AND r.tool_id=:tool_id
		LIMIT 1
		', [
			'path' => '/' . ltrim( $path, '/' ),
			'version' => 'latest',
			'lang' => $lang,
			'tool_id' => $tool->getId(),
		] )->fetchAssociative();

		return new Page(
			$pageData['version'] ?? '',
			$pageData['lang'] ?? $lang,
			$pageData['path'] ?? $path,
			$pageData['contents'] ?? ''
		);
	}

	/**
	 * Get all paths of the given tool, each with a list of available langs.
	 *
	 * @param Tool $tool
	 * @return string[][] Keys are the paths, values are arrays of lang codes.
	 */
	public function getPagePaths( Tool $tool ): array {
		$results = $this->db->executeQuery( '
			SELECT d.lang, SUBSTR( p.path, 2 ) AS path
				FROM pages p
				JOIN docs d ON ( p.doc_id = d.id )
				JOIN repositories r ON ( d.repository_id = r.id )
			WHERE r.tool_id=:tool_id AND d.version="latest"
			GROUP BY p.path, d.lang
			ORDER BY p.path DESC
			;', [
				'tool_id' => $tool->getId(),
			] )->fetchAllAssociative();
		$out = [];
		foreach ( $results as $result ) {
			if ( !isset( $out[ $result['path'] ] ) ) {
				$out[ $result['path'] ] = [];
			}
			$out[ $result['path'] ][] = $result['lang'];
		}
		return $out;
	}

	/**
	 * @param Tool $tool
	 * @return string[]
	 */
	public function getToolsLangs( Tool $tool ): array {
		$sql = 'SELECT lang FROM docs d JOIN repositories r ON d.repository_id=r.id WHERE tool_id = ? GROUP BY lang';
		return $this->db->executeQuery( $sql, [ $tool->getId() ] )->fetchFirstColumn();
	}
}
