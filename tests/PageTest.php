<?php

namespace App\Test;

use App\Page;
use PHPUnit\Framework\TestCase;

class PageTest extends TestCase {

	/**
	 * @dataProvider provideToc
	 * @covers \App\Page::buildToc()
	 */
	public function testToC( $paths, $toc ) {
		$doc = new Page( 'latest', 'en', 'index', '' );
		$this->assertSame( $toc, $doc->buildToc( $paths ) );
	}

	public function provideToc(): array {
		return [
			'top-levels-only' => [
				[ 'index', 'lorem' ],
				[
					[
						'path' => 'index',
						'name' => 'index',
						'active' => true,
						'children' => [],
					],
					[
						'path' => 'lorem',
						'name' => 'lorem',
						'active' => false,
						'children' => [],
					]
				],
			],
			'sublevels' => [
				[ '/lorem', '/lorem/foo', '/lorem/bar' ],
				[
					[
						'path' => '/lorem',
						'name' => 'lorem',
						'active' => false,
						'children' => [
							[
								'path' => '/lorem/foo',
								'name' => 'foo',
								'active' => false,
								'children' => [],
							],
							[
								'path' => '/lorem/bar',
								'name' => 'bar',
								'active' => false,
								'children' => [],
							]
						],
					],
				],
			],
		];
	}
}
