<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220509041700 extends AbstractMigration {

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Add docs and pages tables';
	}

	/**
	 * @param Schema $schema
	 */
	public function up( Schema $schema ): void {
		$this->addSql(
			'CREATE TABLE IF NOT EXISTS `docs` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`tool_id` INT(10) UNSIGNED NOT NULL,
				`lang` VARCHAR(10) NOT NULL,
				`version` VARCHAR(50) NOT NULL,
				FOREIGN KEY ( `tool_id` ) REFERENCES `tools` ( `id` ) ON DELETE CASCADE,
				UNIQUE INDEX UNIQ_DOCS_TOOL_LANG_VERSION ( `tool_id`, `lang`, `version` )
			) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;'
		);
		$this->addSql(
			'CREATE TABLE IF NOT EXISTS `pages` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`doc_id` INT(10) UNSIGNED NOT NULL,
				`path` VARCHAR(150) NOT NULL,
				`contents` TEXT NOT NULL,
				FOREIGN KEY ( `doc_id` ) REFERENCES `docs` ( `id` ) ON DELETE CASCADE,
				UNIQUE INDEX UNIQ_PAGES_PATH ( `doc_id`, `path` )
			) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;'
		);
	}

	/**
	 * @param Schema $schema
	 */
	public function down( Schema $schema ): void {
		$this->addSql( 'DROP TABLE IF EXISTS `pages`' );
		$this->addSql( 'DROP TABLE IF EXISTS `docs`' );
	}
}
