<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// phpcs:ignore MediaWiki.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController {

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/search", name="search")
	 * @param Connection $connection
	 * @param Request $request
	 * @return Response
	 */
	public function search( Connection $connection, Request $request ): Response {
		$keywords = $request->get( 'keywords' );
		$sql = "
			SELECT
				t.title, t.name AS tool_name, t.description, d.lang, d.version, SUBSTR(p.path, 2) AS path, p.contents
			FROM pages p
				JOIN docs d ON ( p.doc_id = d.id )
				JOIN repositories r ON ( d.repository_id = r.id )
				JOIN tools t ON ( r.tool_id = t.id )
 			WHERE
				MATCH (t.title) AGAINST (:search)
				OR MATCH (t.description) AGAINST (:search)
				OR MATCH (p.contents) AGAINST (:search)";
		$results = $connection
			->executeQuery( $sql, [ 'search' => $keywords ] )
			->fetchAllAssociative();
		$pattern = '/.*\n?.*\n?.*\n.*' . preg_quote( $keywords ) . '.*\n?.*\n?/m';
		foreach ( $results as &$res ) {
			if ( stripos( $res['contents'], $keywords ) ) {
				$res['matched'] = 'contents';
				preg_match( $pattern, $res['contents'], $matches );
				$res['context'] = $matches[0] ?? '';
				// @TODO Render Markdown to HTML.
			} elseif ( stripos( $res['title'], $keywords ) ) {
				$res['matched'] = 'title';
			} elseif ( stripos( $res['description'], $keywords ) ) {
				$res['matched'] = 'description';
			}
		}
		return $this->render( 'search.html.twig', [ 'results' => $results ] );
	}
}
