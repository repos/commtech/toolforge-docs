---
title: Translating
---
Every documentation page can be translated into any other language.
Translations will be sent to the tool maintainers via GitLab,
where they will be able to approve them.

## How to translate a page

1. Log in.
2. Navigate to the page that you want to translate, in the language you want to translate *from*.
3. Make sure that your interface language is set to whatever language you want to translate *to* (the 'target language').
4. Click the blue edit button at the top right of the page.
5. Translate the page, or bring it up to date with any changes that have been made to the source page.
6. Save your changes.

This will create a "Merge Request" on GitLab,
which the tool maintainers will merge.
After the changes have been merged they will appear in ToolDocs.

If you want to make further changes to the page,
or to translate other pages,
this can be done at any time.
If a current Merge Request is open for your changes,
subsequent edits will be added to it so that they can all be merged at once.
