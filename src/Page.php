<?php

namespace App;

use League\CommonMark\Environment;
use League\CommonMark\Extension\CommonMarkCoreExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\MarkdownConverter;
use Symfony\Component\Yaml\Yaml;

class Page {

	/** @var string */
	private $version;

	/** @var string */
	private $lang;

	/** @var string */
	private $path;

	/** @var string */
	private $contents;

	/** @var mixed[] */
	private $contentsParts;

	/**
	 * @param string $version
	 * @param string $lang
	 * @param string $path
	 * @param string $contents
	 */
	public function __construct( string $version, string $lang, string $path, string $contents ) {
		$this->version = $version;
		$this->lang = $lang;
		$this->path = $path;
		$this->contents = $contents;
	}

	/**
	 * @return string
	 */
	public function getVersion(): string {
		return $this->version;
	}

	/**
	 * @return string
	 */
	public function getLang(): string {
		return $this->lang;
	}

	/**
	 * Get the path, never with a leading slash.
	 *
	 * @return string
	 */
	public function getPath(): string {
		return ltrim( $this->path, '/' );
	}

	/**
	 * @return string
	 */
	public function getContents(): string {
		return $this->contents;
	}

	/**
	 * @param string $contents
	 */
	public function setContents( string $contents ): void {
		$this->contents = $contents;
		$this->contentsParts = null;
	}

	/**
	 * @return mixed[]
	 */
	public function getMetadata(): array {
		return $this->processContents()['metadata'];
	}

	/**
	 * @return string
	 */
	public function getHtml(): string {
		return $this->processContents()['html'];
	}

	/**
	 * @return mixed[]
	 */
	private function processContents(): array {
		if ( $this->contentsParts ) {
			return $this->contentsParts;
		}
		$markdownEnv = new Environment( [
			'html_input' => 'strip',
			'allow_unsafe_links' => false,
		] );
		$markdownEnv->addExtension( new GithubFlavoredMarkdownExtension() );
		$markdownEnv->addExtension( new CommonMarkCoreExtension() );
		$converter = new MarkdownConverter( $markdownEnv );
		$contents = $this->getContents();
		// Extract frontmatter if it's there.
		// @TODO thephpleague/commonmark has FrontMatterExtension that we can switch to when Toolforge uses PHP 7.4.
		if ( substr( $contents, 0, 3 ) === '---' ) {
			$frontmatterClosePos = strpos( $contents, '---', 3 );
			$frontmatterData = trim( substr( $contents, 3, $frontmatterClosePos - 3 ) );
			$metadata = Yaml::parse( $frontmatterData );
			$bodyData = substr( $contents, $frontmatterClosePos + 3 );
			$html = $converter->convertToHtml( $bodyData );
		} else {
			$metadata = [];
			$html = $converter->convertToHtml( $contents );
		}
		$this->contentsParts = [
			'html' => $html,
			'metadata' => $metadata,
		];
		return $this->contentsParts;
	}

	/**
	 * @param string[] $paths
	 * @return mixed[]
	 */
	public function buildToc( array $paths ): array {
		// Kudos to Nigel Ren https://stackoverflow.com/a/53322666/99667
		$tree = [];
		foreach ( $paths as $path ) {
			$parts = array_filter( explode( '/', $path ) );
			$node = &$tree;
			foreach ( $parts as $level ) {
				$newNode = array_search( $level, array_column( $node, 'name' ) ?? [] );
				if ( $newNode === false ) {
					$newNode = array_push( $node, [
						'path' => $path,
						'name' => $level,
						'active' => $path === $this->getPath(),
						'children' => [],
					] ) - 1;
				}
				$node = &$node[ $newNode ][ 'children' ];
			}
		}
		return $tree;
	}
}
